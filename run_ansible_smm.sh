
#!/bin/bash
#
#  run_ansible.sh - runs an ansible script and pushes the output to out_files
#
base="/home/cp_user/rsync/out_files/"
LOCO_ID=`awk  -F= '/locomotive/{print $2}' < /home/cp_user/locomotive.id`
dt=$(date '+%Y-%m-%d-%H-%M-%S-')
file=${dt}${LOCO_ID}"-SMM.log"
p=${base}${file}
path="`echo ${p}|tr -d '[:blank:]'`"

echo "Making ${base}"
sudo mkdir -p ${base}
echo "touch'ing ${path}"
sudo touch ${path}
echo "chown'ing ${base}"
sudo -H chown -R cp_user:cp_user ${base}
if [ -w ${path} ] ;then
  sudo -H /usr/bin/ansible-playbook -vv /home/cp_user/ansible/smm.yml -e 'ansible_python_interpreter=/usr/bin/python' 2>&1 |tee -a ${path}
else
  echo "Log path [ ${path} ] does not exist. Exiting."
fi
