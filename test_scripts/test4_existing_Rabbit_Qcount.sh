#!/bin/bash
# Returns the integer number of queues declared to the RabbitMQ broker
#

python2 /home/cp_user/ansible/files/rabbitmqadmin.py show overview \
|awk '/START/{if (NR!=1)print "";next}{printf "%s ",$8}END{print "";}' \
|sed -r 's/([^0-9]*([0-9]*)){1}.*/\2/'
