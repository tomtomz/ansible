#!/bin/bash
# Refreshes installed Ubuntu keys.
#

apt-key update|awk -F ':' '/key/{ print $2 }' |sed "s/[ ,a-z]//g" |xargs apt-key adv --recv-key --keyserver keyserver.ubuntu.com
exit 0