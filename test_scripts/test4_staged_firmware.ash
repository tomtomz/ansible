#!/bin/env ash

WATCHME='/usr/local/var/state/upgrade_watcher_d'
UPLOAD='/usr/local/upload'
HED=`date +"%T"` 
TAY=""
FW=`find ${UPLOAD} -mindepth 2`

echo "${HED} ${WATCHME} will be parsed ${TAY}"
echo "${HED} Details from last successful firmware upgrade follow: ${TAY}"
echo "*"
awk '/option/{print "", $2, $3, $4; if (NF >= 3) print "*"}' ${WATCHME} \
        |sed -e "s/[']//"  -e "s/[']//"

if [ -d "${FW}" ]; then
 echo "${HED} ${FW} exists to stage a firmware file ${TAY}"
else
 echo "${HED} Exit Status (1) / no staging directory ${TAY}"
 exit 1
fi

if [ ${UPLOAD} -ot ${WATCHME} ]; then
 echo "${HED} ${WATCHME} is newer than ${UPLOAD} ${TAY}"
 echo "${HED} No firmware upgrade is pending"
 exit 0
else
 echo "${HED} ${UPLOAD} is newer than ${WATCHME} ${TAY}"
 echo "${HED} A firmware upgrade is pending"
 exit 1
fi