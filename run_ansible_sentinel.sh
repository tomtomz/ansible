#!/usr/bin/env bash
#
#  run_ansible.sh - runs an ansible script and pushes the output to out_files
#
base="/home/cp_user/rsync/out_files/"
LOCO_ID=`awk  -F= '/locomotive/{print $2}' < /home/cp_user/locomotive.id`
dt=$(date '+%Y-%m-%d-%H-%M-%S-')
file=${dt}${LOCO_ID}"-SENTINEL.log"
p=${base}${file}
path="`echo ${p}|tr -d '[:blank:]'`"

dpkg --list |awk '$1 ~ /rc/{print $2}' |xargs dpkg -P 2>/dev/null
sudo -H /usr/bin/pip install --upgrade pip
sudo -H dpkg --configure -a

PB="/usr/bin/ansible-playbook"
if [ -e ${PB} ] ;then
  echo "Ansible exists"
else
  echo "Ansible does not exist"
#  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
#  /usr/bin/python3 get-pip.py --user
fi

# update-manager
# update-alternatives --set python3 /usr/bin/python3.5
# sudo apt install --reinstall python3-software-properties
# sudo dpkg --configure -a

echo "Making ${base}"
sudo mkdir -p ${base}
echo "touch'ing ${path}"
sudo touch ${path}
echo "chown'ing ${base}"
sudo -H chown -R cp_user:cp_user ${base}
if [ -w ${path} ] ;then
  sudo -H /usr/bin/ansible-playbook -v /home/cp_user/ansible/sentinel.yml 2>&1 |tee -a ${path}
else
  echo "Log path [ ${path} ] does not exist. Exiting."
fi
ac