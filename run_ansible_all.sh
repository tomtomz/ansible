#!/bin/bash
#
#  run_ansible_all.sh - runs an ansible script and pushes the output to out_files
#
base="/home/cp_user/rsync/out_files/"
LOCO_ID=`awk  -F= '/locomotive/{print $2}' < /home/cp_user/locomotive.id`
dt=$(date '+%Y-%m-%d-%H-%M-%S-')
file=${dt}${LOCO_ID}"-ALL.log"
path=${base}${file}
echo ${path}

if [ -f "/home/cp_user/locomotive.id" ] ;
then
    echo "Located '/home/cp_user/locomotive.id' file." 
else
    echo "Please create a '/home/cp_user/locomotive.id' file now, and run the playbook again." 
    exit 1
fi
echo "Making ${base}"
sudo mkdir -p ${base}
echo "chown'ing ${base}"
sudo chown -R cp_user:cp_user ${base}
echo "touch'ing ${file}"
sudo touch ${file}



sudo -H ssh-copy-id root@10.255.255.253
sudo -H ssh-copy-id root@10.255.255.254
sudo -H ssh-copy-id root@10.255.255.219

# Declare a string
PB_1="/home/cp_user/ansible/sentinel.yml"
PB_2="/home/cp_user/ansible/smm.yml"
PB_3="/home/cp_user/ansible/wcm.yml"
PB_4="/home/cp_user/ansible/hpeap.yml"
PB_5=""
#PB_5="/home/cp_user/ansible/customercode.yml"

# Order the playbook sequence
combine=( $PB_1 $PB_2 $PB_3 $PB_4 $PB_5 )

for arrItem in ${combine[@]}
do
   echo 

   sudo -H /usr/bin/ansible-playbook -D --ssh-common-args '-C -2' "${arrItem}" | tee -a ${path}
done
